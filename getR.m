function [ R ] = getR(x, alphas, C, k, s, AK)
%GETB Calculation hypersphere radius
%   Calculation if the radius of the hypersphere enclosing the data points
R=0;
N=size(x,1);
n=0;
for i=1:N
    if(alphas(i)>0 && alphas(i)<C)
        sums=0;
        for j=1:N
            sums=sums+alphas(j)*k(x(j,:),x(i,:),s);
        end
        R=R+k(x(i,:),x(i,:),s)-2*sums+AK;
        n=n+1;
    end
end
R=R/n;
if (isnan(R))
    R=0;
end

