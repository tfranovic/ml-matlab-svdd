function [ f ] = decide(y, x, alphas, R, AK, k, s)
%DECIDE Decision function for SVDD
%   Decision function for a support vector data description
N=size(y,1);
M=size(x,1);
f=zeros(N,1);
for i=1:N
    f(i)=0;
    for j=1:M
        if(alphas(j)>0)
            f(i)=f(i)+alphas(j)*k(x(j,:),y(i,:),s);
        end
    end
    f(i)=2*f(i)+R-k(y(i,:),y(i,:),s)-AK;
end
end
