clear;
x=load('toy_data_train_X.txt');
y=load('toy_data_train_y.txt');
N=size(x,1);
mu = mean(x); sigma = std(x);
x = (x - repmat(mu, N, 1)) ./ repmat(sigma, N, 1);
x1=x(y==1,:);
x2=x(y==-1,:);

clear N mu sigma x y ans;