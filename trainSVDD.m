function [x, alphas, R, AK, k] = trainSVDD(x, C, type, s)
%TRAINSVDD Training SVDD for data description
if(strcmp(type,'linear'))
    k=@k_lin;
    s=0;
else
    k=@k_gau;
end
%get K matrix
K=getK(x,k,s);

%get alphas
alphas=getAlphas(K,C);

%precompute AK matrix
AK=precAK(K,alphas);

%get R
R=getR(x,alphas,C,k,s,AK);
end

