loadData;

%linear kernel, C=0.2
C=0.2;
figure();
hold on;
axis square;
[x, alphas, R, AK, k] = trainSVDD(x1, C, 'linear');
f1x=plotSVDD(x,alphas,R,AK,k,0,'+b','Ok','-b',2);
[x, alphas, R, AK, k] = trainSVDD(x2, C, 'linear');
f2x=plotSVDD(x,alphas,R,AK,k,0,'+r','Ok','-r',2);
[X1,X2]=meshgrid(-2:0.01:2,-2:0.01:2);
contour(X1,X2,f1x-f2x,'-k','LevelList',[0],'LineWidth',2);


%radial kernel, C=0.2, s={1,2,4,8}
S=[1,2,4,8];
for s=S
    figure();
    hold on;
    axis square;
    [x, alphas, R, AK, k] = trainSVDD(x1, C, 'gaussian',s);
    f1x=plotSVDD(x,alphas,R,AK,k,s,'+b','Ok','-b',2);
    [x, alphas, R, AK, k] = trainSVDD(x2, C, 'gaussian',s);
    f2x=plotSVDD(x,alphas,R,AK,k,s,'+r','Ok','-r',2);
    [X1,X2]=meshgrid(-2:0.01:2,-2:0.01:2);
    contour(X1,X2,f1x-f2x,'-k','LevelList',[0],'LineWidth',2);
end