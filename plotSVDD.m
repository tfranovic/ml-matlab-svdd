function [ f1x ] = plotSVDD(x,alphas,R,AK,k,s,l1,l2,l3,l4)
scatter(x(:,1),x(:,2),l1);
scatter(x(alphas>0,1),x(alphas>0,2), l2);
[X1,X2]=meshgrid(-2:0.01:2,-2:0.01:2);
f1x=zeros(size(X1,1), size(X2,1));
for i=1:size(X1,1)
    f1x(:,i)=decide([X1(:,i),X2(:,i)],x,alphas,R,AK,k,s);
end
contour(X1,X2,f1x,l3,'LevelList',[0],'LineWidth',l4);
end