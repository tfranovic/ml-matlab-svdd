function [ AK ] = precAK(K,alphas)
%PRECAK Precomputing of the AK matrix
AK=sum(sum(K.*(alphas*alphas')));
end

